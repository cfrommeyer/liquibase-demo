package demo.model;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PersonDaoImpl implements PersonDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public void savePerson(Person person) {
        if (em.contains(person)) {
            em.merge(person);
        } else {
            em.persist(person);
        }
    }

    @Override
    @Transactional
    public Person loadPerson(long id) {
        return em.find(Person.class, id);
    }

}
