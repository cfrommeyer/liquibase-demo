package demo.sample;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import demo.model.Person;
import demo.model.PersonDao;

@ContextConfiguration(locations = { "/database-context.xml", "/application-context.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class InsertFindTest {

    private static final String FIRST_NAME = "Hans";
    private static final String LAST_NAME = "M�ller";

    @Autowired
    private PersonDao personDao;

    @Test
    public void testInsertName() {
        Person person = new Person();
        person.setName(LAST_NAME);
        personDao.savePerson(person);
        assertEquals(LAST_NAME, personDao.loadPerson(person.getId()).getName());
    }

    @Test
    public void testInsertNameFull() {
        Person person = new Person();
        person.setName(LAST_NAME);
        person.setFirstName(FIRST_NAME);
        personDao.savePerson(person);
        Person dbPerson = personDao.loadPerson(person.getId());
        assertEquals(LAST_NAME, dbPerson.getName());
        assertEquals(FIRST_NAME, dbPerson.getFirstName());
    }

}
