package demo.model;

import org.springframework.transaction.annotation.Transactional;

public interface PersonDao {

    @Transactional
    void savePerson(Person person);

    @Transactional
    Person loadPerson(long id);

}
