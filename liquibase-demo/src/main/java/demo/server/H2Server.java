package demo.server;

import java.sql.SQLException;

import org.h2.tools.Server;

public class H2Server {

    public static void main(String[] args) throws SQLException {
        Server tcpSserver = Server.createTcpServer(args);
        tcpSserver.start();
        Server webServer = Server.createWebServer(args);
        webServer.start();
        System.out.println("Server up and running!");
    }

}
